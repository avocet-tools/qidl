package main

import (
   "github.com/spf13/cobra"
   "github.com/spf13/viper"
   "gitlab.com/avocet-tools/qidl"
)

var cmd = cobra.Command{
   Use: "qidl",
   Short: "A Query Tool for IDL Files",
}

var query = cobra.Command{
   Use: "query",
   Short: "Queries the Qidl cache for information",
   Run: func(_ *cobra.Command, args []string){
      qidl.RunQuery(args)
   },
}

var sync = cobra.Command{
   Use: "sync",
   Short: "Synchronize IDL files with JSON cache",
   Run: func(_ *cobra.Command, args []string){
      qidl.RunSync(args)
   },
}

func init(){
   viper.SetDefault("qidl.cache", "qidl.json")
   viper.SetDefault("qidl.path", ".")

   // Read Configuration
   viper.SetConfigName("qidl.yml")
   viper.AddConfigPath(".")
   viper.AddConfigPath("$HOME/.config/avocet")
   viper.SetConfigType("yaml")
   err := viper.ReadInConfig()
   if _, ok := err.(viper.ConfigFileNotFoundError); ok {
   } else if err != nil {
      panic(err)
   }

   cmd.PersistentFlags().StringP(
      "wd", "c", 
      viper.GetString("qidl.path"),
      "Path to working directory")
   viper.BindPFlag("qidl.path", cmd.PersistentFlags().Lookup("wd"))
}

func main(){
   cmd.AddCommand(&sync)
   cmd.AddCommand(&query)
   cmd.Execute()
}
