package qidl

import (
   "fmt"
   "regexp"
   "sort"
   "strings"
   "github.com/charmbracelet/log"
   "github.com/charmbracelet/lipgloss"
)

var (
   entry = lipgloss.NewStyle().Bold(true).PaddingTop(2)
   setAt = lipgloss.NewStyle().Underline(true)
   descr = lipgloss.NewStyle().MarginLeft(4).MarginRight(4).Width(90)
)

type Parameter struct {
   Key string
   Descr string `yaml:"description" json:"description"`
   Set any `yaml:"set_at,flow" json:"set_at"`
}
func (i *Idl) findParamMatch(re *regexp.Regexp) ([]*Parameter, bool) {
   params := []*Parameter{}
   for key, param := range i.Parameters {
      if re.MatchString(key) {
         param.Key = key
         params = append(params, param)
      }
   }
   return params, len(params) > 0
}

func (i *Idl) QueryParameters(ps []string){
   if len(ps) == 0 {
      fmt.Println("Parameters:")
      keys := []string{}
      for key := range i.Parameters {
         keys = append(keys, key)
      }
      sort.Strings(keys)
      for _, key := range keys {
         fmt.Println("- ", key)
      }
   }
   params := []*Parameter{}
   for _, key := range ps {
      param, ok := i.Parameters[key]
      if !ok {
         re, err := regexp.Compile(key)
         if err != nil {
            log.Error("RegEx Error: ", err)
            continue
         }
         nparams, ok := i.findParamMatch(re)
         if !ok {
            log.Error("Unknown Key: ", key)
            continue
         }
         params = append(params, nparams...)
         continue
      }
      param.Key = key
      params = append(params, param)
   }
   for _, param := range sortParams(params) {
      param.Print()
   }
}
func sortParams(params []*Parameter) []*Parameter {
   for i := range params {
      for j := range params {
         if strings.Compare(params[i].Key, params[j].Key) > 0 {
            param := params[i]
            params[i] = params[j]
            params[j] = param
         }
      }
   }
   return params
}

func (p *Parameter) GetSet() string {
   return fmt.Sprint(p.Set)
}

func (p *Parameter) Print() {
   fmt.Printf(
      "%s -(%s)\n%s\n",
      entry.Render(p.Key),
      setAt.Render(fmt.Sprint(p.Set)),
      descr.Render(p.Descr),
   )
}

