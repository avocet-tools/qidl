package qidl

import (
   "maps"
)

type Idl struct {
   File string `yaml:"-"`
   Parameters map[string]*Parameter `yaml:"server_parameters" json:"server_parameters"`
}

func (i *Idl) Merge(j *Idl){
   maps.Copy(i.Parameters, j.Parameters)
}

