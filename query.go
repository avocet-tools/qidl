package qidl

func RunQuery(args []string){
   switch len(args) {
   case 0:
      return
   case 1:
      Query(args[0], []string{})
   default:
      Query(args[0], args[1:])
   }
}


func Query(target string, args []string){
   idl := Read()
   switch target {
   case "parameter", "parameters", "params", "param":
      idl.QueryParameters(args)
   }
}
