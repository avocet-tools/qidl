package qidl

import (
   "os"
   "encoding/json"
   "path/filepath"
   "github.com/charmbracelet/log"
   "github.com/spf13/viper"
   "gopkg.in/yaml.v3"
)

func findFiles(f string) []string {
   fs := []string{}
   fi, err := os.Stat(f)
   if err != nil {
      log.Error("Unable to stat file ", f, ": ", err)
      return fs
   }
   if fi.IsDir() {
      nfs, err := os.ReadDir(f)
      if err != nil {
         log.Error("Unable to read directory ", f, ": ", err)
         return fs
      }
      for _, nf := range nfs {
         fs = append(fs, findFiles(filepath.Join(f, nf.Name()))...)
      }
   } else {
      if filepath.Ext(f) == ".idl" {
         fs = append(fs, f)
      }
   }

   return fs
}


func RunSync(args []string){
   fs := findFiles(viper.GetString("qidl.path"))
   log.Infof("Found %d .idl files", len(fs))

   idls := []*Idl{}
   for _, f := range fs {
      con, err := os.ReadFile(f)
      if err != nil {
         log.Error("Unable to read file ", f, ": ", err)
         continue
      }
      idl := &Idl{File: f}
      err = yaml.Unmarshal(con, idl)
      if err != nil {
         log.Error("Error unmarshalling YAML in ", f, ": ", err)
         continue
      }
      idls = append(idls, idl)
   }
   idlSize := len(idls)
   log.Infof("Parsed %d Idls", idlSize)
   if idlSize == 0 {
      return
   }

   idl := idls[0]
   if idlSize > 1 {
      for _, jdl := range idls[1:] {
         idl.Merge(jdl)
      }
   }
   con, err := json.Marshal(idl)
   if err != nil {
      log.Error("JSON Marshal Error: ", err)
      return
   }
   err = os.WriteFile(viper.GetString("qidl.cache"), con, 0444)
   if err != nil {
      log.Error(err)
   }
}

func Read() *Idl {
   con, err := os.ReadFile(viper.GetString("qidl.cache"))
   if err != nil {
      log.Fatal("Error reading cache: ", err)
   }
   idl := &Idl{}
   err = json.Unmarshal(con, idl)
   if err != nil {
      log.Fatal("Error unmarshaling cache: ", err)
   }
   return idl
}
